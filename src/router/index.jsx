import { useDispatch, useSelector } from "react-redux";
import { Navigate, Route, Routes } from "react-router-dom";
import MainLayout from "../layouts/MainLayout";
import DashboardPage from "../views/Dashboard";
import { useEffect, useState } from "react";
import { websiteActions } from "../store/website/website.slice";
import CardsPage from "../views/Cards";

const Router = () => {
  const dispatch = useDispatch();
  const isAuth = useSelector((state) => state.auth.isAuth);
  const [list, setList] = useState([]);
  const [routes, setRoutes] = useState([]);

  function getPath(path, child = {}) {
    let newPath = "/object/" + path;
    let childPath = "";
    if (child?.parent) {
      newPath = "/object/" + child.parent;
      childPath = `/object/${child.parent}/child/${path}`;
    }

    let obj = {
      id: newPath,
      permission: newPath,
      title: newPath,
      children: [],
    };
    if (childPath) {
      obj.child = {
        id: childPath,
        permission: childPath,
        title: childPath,
      };
    }

    if (!list?.includes(newPath)) {
      setRoutes((prev) => [...prev, obj]);
      setList((prev) => [...prev, childPath?.id ? childPath : newPath]);
    }
    return childPath ? childPath : newPath;
  }

  useEffect(() => {
    if (!routes?.length) return;
    let mergedObjects = {};

    routes?.forEach((object) => {
      if (mergedObjects[object.permission]) {
        mergedObjects[object.permission].children.push(object.child);
      } else {
        mergedObjects[object.permission] = {
          id: object.id,
          permission: object.permission,
          title: object.title,
          children: [object.child],
        };
      }
    });

    mergedObjects = Object.values(mergedObjects);

    dispatch(websiteActions.setPermissions({ list: mergedObjects }));
  }, [routes]);

  // if (!isAuth)
  //   return (
  //     <Routes>
  //       <Route path="/" element={<AuthLayout />}>
  //         <Route index element={<Navigate to="/login " />} />
  //         <Route path="login" element={<Login />} />
  //         <Route path="registration" element={<Registration />} />
  //         <Route path="*" element={<Navigate to="/login" />} />
  //       </Route>
  //       <Route path="*" element={<Navigate to="/login" />} />
  //     </Routes>
  //   );
  return (
    <Routes>
      <Route path="/" element={<MainLayout />}>
        <Route index element={<Navigate to={getPath("dashboard")} />} />
        <Route path={getPath("dashboard")} element={<DashboardPage />} />
        <Route path={getPath("cards")} element={<CardsPage />} />

        <Route path="*" element={<Navigate to={getPath("dashboard")} />} />
      </Route>
    </Routes>
  );
};

export default Router;
