import request from "../../utils/request";

const productService = {
  getList: (params) => request.get("/products", { params })
};

export default productService;
