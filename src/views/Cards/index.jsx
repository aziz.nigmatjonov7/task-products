import FormCard from "../../components/FormCard";
import Header from "../../components/Header";
import CBreadcrumbs from "../../components/CElements/CBreadcrumbs";
import { useMemo } from "react";
import cls from "./style.module.scss";
import { useSelector } from "react-redux";

const CardsPage = () => {
  const cardList = useSelector((state) => state.website.card_list);
  const breadCrumbItems = useMemo(() => {
    return [
      {
        label: "Products",
        link: -1,
      },
    ];
  }, []);
  return (
    <>
      <Header title="Cards">
        <CBreadcrumbs items={breadCrumbItems} progmatic={true} type="link" />
      </Header>
      <FormCard minHeight="70vh">
        {cardList?.length ? (
          <ul className={cls.list}>
            {cardList.map((item, index) => (
              <li key={index}>{item.title}</li>
            ))}
          </ul>
        ) : (
          <img width={200} src="/images/no-data.png" alt="no data" />
        )}
      </FormCard>
    </>
  );
};

export default CardsPage;
