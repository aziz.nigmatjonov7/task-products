import Header from "../../components/Header";
import FormCard from "../../components/FormCard";
import CTable from "../../components/CElements/CTable";
import { useEffect, useMemo, useState } from "react";
import usePageRouter from "../../hooks/useObjectRouter";
import { useGetQueries } from "../../hooks/useQueries";
import axios from "axios";
import cls from "./style.module.scss";
import SearchInput from "../../components/SearchInput";
import { useDispatch, useSelector } from "react-redux";
import { websiteActions } from "../../store/website/website.slice";
import SaveButton from "../../components/Buttons/SaveButton";

const DashboardPage = () => {
  const { navigateTo } = usePageRouter();
  const { currentPage, currentLimit, search } = useGetQueries();
  const [list, setList] = useState([]);
  const [total, setTotal] = useState(1);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const cardList = useSelector((state) => state.website.card_list);

  const params = useMemo(() => {
    const currentpage = parseInt(currentPage) || 1;
    const limit = parseInt(currentLimit) || 20;
    let result = {};
    result.skip = (currentpage - 1) * 10;
    result.limit = limit;
    result.search = search;

    return result;
  }, [currentPage, currentLimit, search]);

  const getProducts = (params) => {
    setLoading(true);
    setList([]);
    axios
      .get(
        `https://dummyjson.com/products/search?skip=${params.skip}&limit=${
          params.limit
        }&q=${search ?? []}`,
        params
      )
      .then((res) => {
        setList(res?.data?.products);
        setTotal(res?.data?.total);
      })
      .finally(() => setLoading(false));
  };

  const handleDeleteProduct = (id) => {
    setLoading(true);
    axios
      .delete(`https://dummyjson.com/products/${id}`)
      .then((res) => {
        getProducts(params);
      })
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    getProducts(params);
  }, [params]);

  const headColumns = useMemo(() => {
    return [
      {
        id: "index",
        width: 50,
        textAlign: "center",
      },
      {
        title: "Name",
        id: "title",
      },
      {
        title: "Brand",
        id: "brand",
      },
      {
        title: "Price",
        id: "price",
        // filter: true,
      },
      {
        title: "Actions",
        id: "actions",
        textAlign: "center",
        click: "custom",
        width: 100,
      },
    ];
  }, []);

  const handleActions = (status, element) => {
    if (status === "delete") {
      handleDeleteProduct(element.id);
    } else {
      const list = [...cardList, element];
      dispatch(websiteActions.setCardList(list));
    }
  };

  return (
    <div className="dashboard">
      <Header
        title="Products"
        extra={
          <div style={{ display: "flex" }}>
            <SaveButton onClick={() => navigateTo(`cards`)} minWidth="auto">
              Go to card
            </SaveButton>
            <div className={cls.btn}>{cardList?.length} products</div>
          </div>
        }
      />
      <FormCard minHeight="70vh">
        <div className={cls.extraHeader}>
          <SearchInput width={500} styles={{ width: "300px" }} />
        </div>
        <CTable
          outerPadding="0"
          headColumns={headColumns}
          bodyColumns={list}
          clickable={true}
          count={total}
          currentPage={currentPage}
          currentLimit={currentLimit}
          isLoading={loading}
          handleActions={handleActions}
          isResizeble={true}
        />
      </FormCard>
    </div>
  );
};

export default DashboardPage;
